
//Regex Code to validate email
const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


const formElement = document.getElementById("form")

const userNameErrorMsg = document.getElementById("user-name")
const emailErrorMsg = document.getElementById("user-email")
const agreeErrorMsg = document.getElementById("agreeErrorMsg")


const userName = document.getElementById("userName")
const emailElement = document.getElementById("e-mail")
const loveElement = document.getElementById("love")
const colorElement = document.getElementById("color-selector")
const ratingElemnt = document.getElementById("range")
const generElements = document.querySelectorAll('input[name="gener"]')

const agreeElement = document.getElementById("agree")


//Handling Name field
userName.addEventListener("blur",(event)=>{
    if(event.target.value === ""){
        userNameErrorMsg.textContent = "Username required*"
        
    }else{
        userNameErrorMsg.textContent = ""
    }
})


//Handling email field
emailElement.addEventListener("blur",(event)=>{
    if(event.target.value.match(validRegex)){
        emailErrorMsg.textContent = ""
    }else{
        emailErrorMsg.textContent = "Enter valid email*"
    }
})


//Modal Elements
const modalElement = document.getElementById("modal")

const nameHeader = document.getElementById("modalUserName")
const modalEmail = document.getElementById("modalEmail")
const modalLove = document.getElementById("modalLove")
const modalColor = document.getElementById("modalColor")
const modalRating = document.getElementById("modalRating")
const modalGener = document.getElementById("modalGener")
const modalTerms = document.getElementById("modalTerms")
const closeButton  = document.getElementById("closeButton")


formElement.addEventListener("submit",(event)=>{
    event.preventDefault()
    
    // Validating Form
    if(agreeElement.checked === false){
        agreeErrorMsg.textContent = "Please click here to Agree to terms and condition*"
    }if(!emailElement.value.match(validRegex)){
        emailErrorMsg.textContent = "Enter valid email*"
    }if(userName.value === ""){
        userNameErrorMsg.textContent = "Username required*"
    }else{

        modalElement.classList.add("displayStylingModal")
        modalElement.classList.remove("modalDisplay")



        //Updating Modal Content
        nameHeader.textContent = `Hello ${userName.value}`
        modalEmail.textContent = `Email: ${emailElement.value}`
        modalLove.textContent = `You Love: ${loveElement.value}`
        modalColor.textContent = `Color: ${colorElement.value}`
        modalRating.textContent = `Rating: ${ratingElemnt.value}`

        for(let gener of generElements){
            if(gener.checked){
                modalGener.textContent = `Book Gener: ${gener.value} ${colorElement.value}`
                break;
            }
        }
        modalTerms.textContent = "You agree to Terms and Conditions"


        //Setting input values to default after submmition of form
        userName.value = ""
        emailElement.value = ""
        ratingElemnt.value = "0"
        colorElement.value = "#000000"
        const fictionInput = document.getElementById("fiction")
        fictionInput.checked = true
        agreeElement.checked = false
        const inputElements = document.querySelectorAll("input")

        for(let item of inputElements){
            item.readOnly = true;
        }
        const spans = document.querySelectorAll("span")

        for(let span of spans){
            span.style.display = "none";
        }

    }

})



//Handling Tearms and Conditions
agreeElement.addEventListener("click",(event)=>{
    if(event.target.checked === true){
        agreeErrorMsg.textContent = ""
    }else{
        agreeErrorMsg.textContent = "Please click here to Agree to terms and condition*"
    }
    
})


// Handling close button actions
closeButton.onclick = ()=>{
    modalElement.classList.remove("displayStylingModal")
    modalElement.classList.add("modalDisplay")
    formElement.classList.remove("hideformElement")
    formElement.classList.add("displayStyling")

    const inputElements = document.querySelectorAll("input")

    for(let item of inputElements){
        item.readOnly = false;
    }
    const spans = document.querySelectorAll("span")

    for(let span of spans){
        span.style.display = "block";
    }

}